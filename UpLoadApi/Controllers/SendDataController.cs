﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UpLoadApi.Models;

namespace UpLoadApi.Controllers
{
    public class SendDataController : ApiController
    {
        [HttpGet]
        public string Get()
        {
            return "Hello!This is SendData!";
        }
        [HttpGet]
        [ActionName("Test")]
        public string GetActionName()
        {
            return "Test";
        }
        [HttpPost]
        public Person GetDataFromStream([FromBody]Person p)
        {
            return p;
        }

        [HttpPost]
        public List<Person> SendListData([FromBody] List<Person> listP)
        {

            return listP;
        }
        [HttpPost]
        public async Task<List<Person>> SendListJsontData()
        {
            List<Person> data = await this.Request.Content.ReadAsAsync<List<Person>>();

           //List<Person> data = JsonConvert.DeserializeObject<List<Person>>(listP);
            return data;
        }
    }
}
